# Docker-Postgres-Adminer-LinuxAppWithVNC-ELK-compose



## My cloud mini-project

I created docker-compose slack contains the following services:


```
- Postgres : database.
- Adminer  : database manager.
- Application : an ubuntu container with GUI, using VNC protocol to show it in the browser using port 6901.
- ELK: elasticsearch-logstash-kibana to visualise the flights database.
```
 - NOTE: please use HTTPS to acccess to the application container: HTTPS://localhost:6901 (username: kasm_user, password: password)

## Screenshots of the application
You can find the application screenshot in the cap directory.
